This project implements a simulation of the Tragedy of the Commmons. It follows pseudo code given in the paper by Robert Axtell.
The simulation consists of two parts. In the first parts, agents are repeatedly optimizing the effort they put into
harvesting a shared resource. The yield is hereby decreasing on scale. 
In the second part, agents are allowed to communicated. In every round, two agents are chosen randomly and we suggest
random values by which they can decrease their efforts. If these changes are beneficial to both agents, they are adopted,
otherwise the changes are rolled back.