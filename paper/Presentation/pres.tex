\documentclass{beamer}
\usetheme{Goettingen}
\usecolortheme{seahorse}
\usefonttheme{structurebold}
%Information to be included in the title page:
\title{Simulating a Decentralized Approach to the Tragedy of the Commons}
\author{Dennis Natusch}
\institute{Mathematics of Sustainability}
\date{June 8, 2021}


\usepackage{multicol}
\usepackage{listings}

\lstset{language=java}

\usepackage{xcolor}
\usepackage{graphicx}

\definecolor{foreground}{RGB}{255,255,255}
\definecolor{background}{RGB}{44,44,44}
\definecolor{title}{RGB}{107,174,214}
\definecolor{gray}{RGB}{155,155,155}
\definecolor{subtitle}{RGB}{102,255,204}
\definecolor{hilight}{RGB}{102,255,204}
\definecolor{vhilight}{RGB}{255,111,207}

%\setbeamercolor{subtitle}{fg=subtitle}
\setbeamercolor{institute}{fg=gray}
\setbeamercolor{navigation bar}{fg=gray}

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}

\lstset{style=mystyle}

\begin{document}


\frame{\titlepage}

\begin{frame}
\frametitle{Original Paper and Code Repository}
\bibliographystyle{plain}
\bibliography{./../bibliography}
\nocite{axtell2009emergence}
\end{frame}

\begin{frame}{Tragedy Of the Commons}
    \begin{itemize}
        \item set of self interested agents shares access to resource
        \item resource can be harvested to gain utility
        \item overusing the resource as a society can cause depletion
    \end{itemize}
\end{frame}

\begin{frame}{Agent Based Modelling}
    \begin{itemize}
        \item simulate collective action as actions of individual agents
        \item agents follow simple rules from which complex collective behaviour emerges
    \end{itemize}
\end{frame}

\begin{frame}{Economic Model}
    \begin{itemize}
        \item set of Agents $A$
        \item individual efforts $e_{i\in A} \in [0,1]$
        \item total effort $E$
        \item total output $O(E) = aE^b$
    \end{itemize}
\end{frame}

\begin{frame}{Economic Model}
Agent $i$'s share of the total output is proportional to its share of total effort, that is
\[
\frac{e_i}{E}O(E) = \frac{ae_i}{(e_i + E_{\sim{i}})^{1-b}}
\]
\end{frame}

\begin{frame}{Utility}
    Agents can derive utility from effort NOT used, we can consider that leisure
    $\rightarrow $ Cobb-Douglas Preference \newline \newline
    \onslide<1->$U^i(e_i;$\onslide<2->$~ \theta_i, E_{\sim{i}}) = $\onslide<3-> $\left( \frac{ae_i}{(e_i + E_{\sim{i}})^{1-b}} \right)$\onslide<5->$^{\theta_i}$ \onslide<4->$(1-e_i)$ \onslide<5->$^{1-\theta_i}$
    
\end{frame}

\begin{frame}{Maximizing the Utility}
    \begin{enumerate}
        \item Derive $U^i(e_i;~ \theta_i, E_{\sim{i}})$ with respect to $e_i$
        \item Set this equal to Zero
        \item Solve for $e_i$
    \end{enumerate}
    
    \begin{equation}
    e_i^* = \frac{b\theta_i - E_{\sim{i}} + \sqrt{ (E_{\sim{i}}-b\theta_i)^2 + 4\theta_i E_{\sim{i}} (1+\theta_i(b-1)) }}{2(1+\theta_i(b-1))}
     \end{equation}
\end{frame}

\begin{frame}{Simulate this}
    \begin{itemize}
        \item initialize all agents to have zero effort
        \item Repeat:
        \begin{itemize}
            \item For $i=1,2,...,n$:
            \begin{itemize}
                \item allow agent i to maximize its effort  
            \end{itemize}
        \end{itemize}

    \end{itemize}
    
\end{frame}

\begin{frame}[t,fragile]{Object Oriented Programming}
    \textbf{class} = a template for objects; describes attributes and functions an object should have
    \newline \newline
    \pause
    \begin{lstlisting}
        public class Agent {
        
            float effort;
            float preference;
            float last_utility;
            AgentPool agentPool;
            
            \\functions 
            ...
        }
        
    \end{lstlisting}
\end{frame}

\begin{frame}[t,fragile]{Object Oriented Programming}
    \textbf{class} = a template for objects; describes attributes and functions an object should have
    \newline
    \begin{lstlisting}
    public class AgentPool {

        //----PARAMETERS---------
        int numberOfAgents = 10;
        int numberOfRounds = 50;
        float preference = 0.5f;
        float a = 0.5f;
        float b = 0.5f;
        //-----------------------
        Agent[] agentList;
        float lastOutput = 0;

        ...
        //functions
        ...
    }
    \end{lstlisting}
\end{frame}    

\begin{frame}{Results}
    - all Agents have exactly the same effort\newline
    - no significant changes happen in the last rounds \newline
    $\Rightarrow$ we reached the Nash Equilibrium
    \pause
    \begin{center}
        \textbf{\Large Can we do better?}
    \end{center}
    
\end{frame}
    
\begin{frame}{Pareto Optimum}
    \begin{itemize}
        \item situation in which no Pareto improvement is possible
        \item if all preferences are the same, a Pareto Optimum has all agents putting in the same effort
        \item we can find the optimum by maximizing the total utility:
        
        \begin{itemize}
            \item Find the derivation $U'(E; \theta)$ of the utility function with respect to $E$.
            \item Set $0 = U'(E; \theta)$ and solve for $E$
            \item Divide the result by the number of agents to get the effort of a single agent
        \end{itemize}
    \end{itemize}

\end{frame}

\begin{frame}{Decentralized Approach}
\begin{itemize}
    \item allow two agents to make agreements to lower their efforts
    \item agents only agree when this change improves their utility
    \item Simulation:
    \begin{itemize}
        \item for x rounds:
        \begin{itemize}
            \item randomly select two agents
            \item propose new, lower efforts for both
            \item if both agents gain, adopt new efforts, otherwise roll back
        \end{itemize}
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Results}
    \begin{itemize}
        \item all (or at least most) agents reduce their efforts $\Rightarrow$ less exploitation of the resource
        \item all agents gain utility $\Rightarrow$ Pareto improvement
        \item the Pareto optimum is not reached \newline \newline
        $\Rightarrow$ even without a central institution controlling the resource, a significant improvement is possible
    \end{itemize}
\end{frame}
    
\begin{frame}{Discussion}
    \begin{itemize}
        \item Questions about the model or the simulation? \pause
        \item Do you want to test the simulation with other Parameters? \pause
        \item Do you see problems with the model or the assumptions it makes? \pause
        \item What do we need to implement this type of decentralized approach? \pause
    \end{itemize}
\end{frame}

    \begin{frame}{Sources and Code Repository}
        \begin{refsection}
            \bibliography{./../bibliography}
            \nocite{*}
        \end{refsection}

    \end{frame}
\end{document}