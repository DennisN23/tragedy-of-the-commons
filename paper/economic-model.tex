In this section, we will analyze an economic model of the Tragedy of the Commons following~\cite{axtell2009emergence}.
More precisely, we will assume a group of self-interested agents that share access to a resource which yields decreasing returns to scale.
An example for such a situation in the context of sustainability could be a population of fish in a sea.
Agents, here trawlers, have an interest to harvest the resource by catching fish.
However, the returns for a given effort are decreasing with the amount of fish in the sea, which decreases with agents harvesting more fish.

We will start with some notations and definitions.
Let the set of agents be $A$ and let $e_{i \in A} \in [0,1]$ be the effort an agent supplies.
The total effort of all agents is denoted as $E$.
The total output of all agents depends on $E$ and is denoted as $O(E)$.
This function has the form \[ O(E) = aE^b\] where $b < 1$ since we defined the returns to be decreasing on scale.
Note that $b=1$ represents a constant return on scale.

We assume that all agents have the same productivity.
This means that an agent's share of the total effort is proportional to its share of the total output, that is
\begin{equation}
\label{eq:shareofoutput}
    \frac{e_i}{E}O(E) = \frac{e_i a E^b}{E} = \frac{a e_i}{E^{1-b}} = \frac{a e_i}{(e_i+E_{\sim{i}})^{1-b}}
\end{equation}
where $E_{\sim{i}} = E - e_i$ is the total effort of all other agents. 

Now we want to define the utility function for each agent.
If we assume that utility is equal to the share of the output received, then each agent will always have the maximum effort, that is $1$.
However, we have to consider that not using effort provides utility as well.
If we consider effort to be the time an agent spends working, then we have to keep in mind that time not working provides utility as well, otherwise an agent would work all the time which is not always desirable.
We assume a Cobb-Douglas preference for putting effort into working and not using the effort by not working.
Thus, for an agent $i$ we get
\begin{equation}
    U^i(e_i;\theta_i,E_{\sim{i}}) = \left( \frac{ae_i}{(e_i + E_{\sim{i}})^{1-b}} \right)^{\theta_i} (1-e_i)^{1-\theta_i}\label{eq:equation}
\end{equation}
Notice that the term in the first parenthesis is the share of the total output that agent $i$ receives as shown in~\eqref{eq:shareofoutput}.
The term in the second parenthesis is the effort not used for working, so here utility is derived from leisure.
The parameter $\theta$ is agent specific and describes the preference an agent has towards working and leisure.
Some agents favor more leisure and derive more utility from it, these have a smaller $\theta$ than agents that favor more work.

Every agent wants to maximize its own utility.
If we don't allow agents to make agreements, the only way for an individual agent to maximize its utility is by optimizing its effort.
In other words, each agent chooses an effort $e_i^*$ such that its utility is maximized, that is
\begin{equation}
    e_i^* = \argmax_{e_i}\left[U_i(e_i;\theta_i,E_{\sim{i}})\right]\label{eq:equation2}
\end{equation}
To find this, we need to derive the function $U^i$ with respect to $e_i$ and set this to zero.
So we need to solve
\begin{align}
    0 &= U_i^{'}(e_i^*;\theta_i,E_{\sim{i}})\\ 
    &=
    - \frac{\left( e_i^*(b\theta_i (e_i^*-1)
    -\theta_i e_i^*+e_i^*)+ E_{\sim{i}}(e_i^*+\theta_i )) \left(ae_i^*(E_{\sim{i}} + e_i^*)^{b-1} \right)\right)^\theta_i}
    {e_i^*(E_{\sim{i}}+e_i^*) (1-e_i^*)^{\theta_i} }
\end{align}

This yields 
\label{optimalUtility}
\begin{equation}
    e_i^* = \frac{b\theta_i - E_{\sim{i}} + \sqrt{ (E_{\sim{i}}-b\theta_i)^2 + 4\theta_i E_{\sim{i}} (1+\theta_i(b-1)) }}{2(1+\theta_i(b-1))}\label{eq:equation3}
\end{equation}

\subsection{Pareto Optimum for Homogeneous Groups}
\label{subsec:pareto-optimum-for-homogeneous-groups}
We can also find the Pareto optimum for a population with equal preferences, so $\theta_i = \theta_j$ for all $j,i$.
In this case, all effort levels will be same.
So we can find the maximum of the total utility.\newline
Let $n$ be the number of agents and $U$ be the total utility of the population.
We can see that in this case, $E = n \cdot e_i$ for every agent $i$.
Every agent has the same utility, so the total utility is
\begin{align}
    U(E;\theta) &= n \cdot \left( \frac{ae_i}{(e_i+E_{\sim{i}})^{1-b}} \right)^{\theta} \cdot (1-e_i)^{1-\theta} \nonumber \\
    &= n \cdot \left( \frac{aE}{n \cdot E^{1-b}} \right)^{\theta} \cdot \left(1-\frac{E}{n}\right)^{1-\theta}  \nonumber\\
    &=  n \cdot \left( \frac{aE^b}{n} \right)^{\theta} \cdot \left(1-\frac{E}{n}\right)^{1-\theta}
\end{align}

We can maximize the utility by deriving it with respect to $E$, setting it equal to zero, solving for $E$ and then assigning each agent the effort $E/n$.
Assuming the parameters $a$, $b$, $\theta$, $E$ and $n$ are positive, we obtain:
 \begin{equation}
     U'(E;\theta) = a^\theta \cdot (n-E)^{-\theta} \cdot E^{b\theta-1} \cdot (b\theta (n-E)+(\theta-1) E)\label{eq:equation4}
 \end{equation}
To find the maximum, we set this equal to zero:
\begin{align}
    0 &= U'(E;\theta) \nonumber \\
    0 &= a^\theta \cdot (n-E)^{-\theta} \cdot E^{b\theta-1} \cdot (b\theta (n-E)+(\theta-1) E) \nonumber \\
    E &= \frac{bn\theta}{b\theta - \theta +1}
\end{align}
For an individual agent, we then get 
\begin{equation}
\label{eq:paretoEffort}
    e_i^* = \frac{b\theta}{b\theta-\theta+1}
\end{equation}

\subsection{Comparing Nash Equilibrium and Pareto Optimum}\label{subsec:comparing-nash-equilibrium-and-pareto-optimum}

In Figure~\ref{fig:paretoVSnash} we plot the effort levels of individual agents as a function of the preference $\theta$, which we assume to be equal among all agents.
We can see that the efforts in the Pareto Optimum are lower than the efforts in the Nash Equilibrium for all preferences.
This means that the shared resource is harvested less, which means it is less likely that overexploitation and depletion of the resource happens.
\begin{figure}[h!]
    \centering
    \includegraphics[width=0.5\textwidth]{desmos-graph.png}
    \caption{Comparing the efforts level of every agent for all preferences.
    The orange line is the Pareto Optimum, the blue line is the Nash Equilibrium.
    We assume a group of 10 agents and $b=0.5$}
    \label{fig:paretoVSnash}
\end{figure}

We can also take a look at the utilities for agents in the Nash Equilibrium and the Pareto Optimum.
We assume again that $b=0.5$, $a=0.5$ and that we have a group of agents with the same preference $\theta = 0.5$.
Then the effort in the Pareto Optimum is $1/3$~ (Equation~$\eqref{eq:paretoEffort})$
while in the Nash Equilibrium, the effort depends on the number of agents $n$ and is
\begin{equation}
    \label{eq:nash_effort}
    e_i = \frac{0.5n-0.25}{n-0.25}.
\end{equation}
The utilities, which are equal for all agents are plotted as a function of the number of agents in Figure~\ref{fig:utility}.

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.5\textwidth]{desmos-graph(1).png}
    \caption{Utility as a function of the number of agents.
    Blue is the Nash Equilibrium, Orange is the Pareto Optimum}
    \label{fig:utility}
\end{figure}

We can see that in the Pareto Optimum, all agents have a higher utility than in the Nash Equilibrium. 
In other words, the Pareto Optimum does not only reduce pressure on the resource by lowering every agent's effort, it also increases each agent's utility compared to the Nash Equilibrium.
Hence, it is in the individual interest of each agent as well as in the common interest to reach the Pareto Optimum or get closer to it.