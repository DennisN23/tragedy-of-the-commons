In this section we will implement a program to simulate the situation described above, following the pseudo code given by~\cite{axtell2009emergence}.
This way we will be able to run our own tests with different parameters.
The results of these will be presented in Section~\ref{sec:result_section}.

We will implement the code in the language Java, however we will do so using as little language specific keywords as possible, to allow everyone to understand the code, even without knowledge of Java.
We will for example omit keywords like \verb|private| or \verb|final| which are not necessary for running the code.
The full implementation can be found on GitLab~\cite{codeRepo}.

In Java, we can create classes, which are templates for objects.
In our case, we want to make every agent an object, allowing us to store all data related to a specific agent in a single object and adding methods to perform computations and changes to this data.
Consider this implementation of an agent in Java:
\newpage
\begin{lstlisting}[label={lst:lstlisting6}]
public class Agent {

    float effort;
    float preference;
    float last_utility;
    AgentPool agentPool;

    public Agent(float pref, AgentPool ap){
        effort = 0;
        preference = pref;
        last_utility = 0;
        agentPool = ap;
    }
}
\end{lstlisting}

Note here that this is not an agent object, it is a class called agent, which is a template for an object and can be used to create objects of the type \verb|Agent|.
This class describes the type of data that is stored in each \verb|Agent| object, for example a floating-point value called \verb|effort|, but it does not assign a value to it, because the value can be different for each object.
The listing also includes our first function, that is an initializer to create an actual object of the type \verb|Agent|.
Here we start by setting all values to zero except for \verb|preference| and \verb|agentPool|, which get assigned whatever the caller passed when calling the initializer.
This function then returns an actual object of the type \verb|Agent|.
We will later add more functions to this class,
but first we will take a look at a second class we will need, the class \verb|AgentPool|:

\begin{lstlisting}[label={lst:lstlisting}]
public class AgentPool {

    //----PARAMETERS---------
    int numberOfAgents = 10;
    int numberOfRounds = 25;
    float preference = 0;
    float a = 0.5f;
    float b = 0.5f;
    //-----------------------

    Agent[] agentList;
    float lastOutput = 0;

    public AgentPool(){
        agentList = new Agent[numberOfAgents];

        for (int i = 0; i<numberOfAgents; i++){
            if (preference == 0) agentList[i] = new Agent(this);
            else agentList[i] = new Agent(preference, this);
        }
    }
}
\end{lstlisting}
Again this is only an excerpt of the full code that includes more functions, some of which will be explained later.
This class describes the pool of all agents, it manages transactions between agents and allows us to compute statistics for the whole population, such as the total effort or output of all agents.
Here we actually assign values to some variables.
These are our parameters that should stay constant throughout the simulation, but can be changed between simulations to test different scenarios.
Note that the variable \verb|preference| can either be set to a non-zero value, in which case every agent is initialized with that preference, or it can be set to zero as in this listing.
In this case, every agent will be assigned a random preference.
In the initializer of that function, we create a number of agents and store pointer to them in an array, allowing us to later interact with those agents. 
Notice again that we have not created an actual object of type \verb|AgentPool| yet, we have merely described how such an object would look like.
We will use this template to create an actual AgentPool later.

Before we can start implementing our simulation, we need to code two functions for our \verb|Agent| class,
one to compute the utility in the current situation and another one to change the effort to maximize the agent's utility according to Equation (\ref{optimalUtility}).
We get the following code:

\begin{lstlisting}[label={lst:lstlisting2}]
public float maximize_utility(){
    float eff_oth = agentPool.getTotalEffort()-effort; 
    float b = agentPool.b;
    effort = (float) ( b * preference - eff_oth
        + Math.sqrt(Math.pow(eff_oth - b * preference, 2)
            + 4*preference * eff_oth * (1+preference*(b-1))))
        / (2 * (1 + preference * (b-1)));
        return effort;
}

public float compute_utility(){
    return (float) (Math.pow( (agentPool.a*effort) 
        / (Math.pow(agentPool.getTotalEffort(), 1-agentPool.b)) , preference)
            * Math.pow(1-effort, 1-preference));
}
\end{lstlisting}

With that, it is easy to run our first simulation in which agents are not permitted to communicate and make agreements.
Instead, in each round, every agent calculates its optimized effort to maximize its utility and changes its effort accordingly.
The code for this function is shown below.
We omit code for plotting to only show the parts important to our simulation:

\begin{lstlisting}[label={lst:lstlisting3}]
public void runSimWithoutCommunication() {
        for (int i=0; i<numberOfRounds; i++){
            for (int j=0; j<numberOfAgents; j++){
                agentList[j].maximize_utility();
            }
        }
}
\end{lstlisting}

Now we only have to implement a \verb|main| function in which we create an \verb|AgentPool| and call \verb|runSimWithoutCommunication()|.
The code is the following:

\begin{lstlisting}[label={lst:lstlisting4}]
public static void main(String[] args) {
        AgentPool pool = new AgentPool();
        pool.runSimWithoutComm();
}
\end{lstlisting}

The results of this are shown in Section~\ref{sec:result_section}.\newline

Now we will write code for the simulation in which we permit two agents to make agreements.
More precisely we will repeatedly select two random agents, propose two random efforts that are lower than the current efforts
and then check whether both agents would gain.
If they do gain from these changes, the changes are applied, otherwise they are rolled back.
The code is shown below, again omitting code related to plotting.

\begin{lstlisting}[label={lst:lstlisting5}]
      public void runSimWithComm() {

        //init random number generator
        Random random = new Random();
        int agent1_index, agent2_index;

        for (int i=0; i<numberOfRounds; i++){

            //assign random numbers, repeat if both indices are equal
            do {
                agent1_index = random.nextInt(numberOfAgents);
                agent2_index = random.nextInt(numberOfAgents);
            } while (agent1_index == agent2_index);


            //propose new effort levels
            double effort_change_1 = random.nextDouble() * step;
            double effort_change_2 = random.nextDouble() * step;

            //store utility before applying effort changes to later evaluate whether effort change improves utility
            double utility1Before = agentList[agent1_index].compute_utility();
            double utility2Before = agentList[agent2_index].compute_utility();

            //change efforts
            agentList[agent1_index].effort -= effort_change_1;
            agentList[agent2_index].effort -= effort_change_2;

            //if either agents loses utility after effort change, roll back effort changes
            if (utility1Before > agentList[agent1_index].compute_utility() || utility2Before > agentList[agent2_index].compute_utility()){
                agentList[agent1_index].effort += effort_change_1;
                agentList[agent2_index].effort += effort_change_2;
            }
        }
    }
\end{lstlisting}

The results of this are shown in Section~\ref{sec:result_section}.