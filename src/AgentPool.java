import org.knowm.xchart.QuickChart;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class AgentPool {

    //-----------------------
    //----PARAMETERS---------
    int numberOfAgents = 10;
    int numberOfRounds = 50000;
    //if preference == 0, every agent is assigned a random preference, otherwise every agent is assigned the same pref
    float preference = 0.5f;
    float a = 5f;
    float b = 0.5f;
    float step = 0.005f;
    int lag_time = 100;
    //-----------------------

    Agent[] agentList;

    //for plotting
    double[] xData = new double[numberOfAgents];
    double[][] yData= new double[numberOfAgents][numberOfAgents];
    double[] yDataPareto = new double[numberOfAgents];
    double[][] utilityData = new double[numberOfAgents][numberOfAgents];
    double[] utilityDataPareto = new double[numberOfAgents];

    XYChart effortChart;
    XYChart utilityChart;

    SwingWrapper<XYChart> effortWrapper;
    SwingWrapper<XYChart> utilityWrapper;

    public AgentPool(){

        //init list of agents
        agentList = new Agent[numberOfAgents];

        //init agents and fill list
        for (int i = 0; i<numberOfAgents; i++) {
            if (preference == 0) agentList[i] = new Agent(this);
            else agentList[i] = new Agent(preference, this);
            xData[i] = i + 1;
            yData[0][i] = 0;
        }
        
        //prepare the plot
        initPlot();
    }

    public void initPlot(){
        //prepare the plotting
        effortChart = QuickChart.getChart("Effort Levels of Agents", "Agents", "Effort", "Without communication",
                xData, yData[0]);
        effortChart.getStyler().setYAxisMax(0.5);
        effortChart.getStyler().setYAxisMin(0.3);
        effortWrapper = new SwingWrapper<>(effortChart);
        effortWrapper.displayChart();

        utilityChart = QuickChart.getChart("Utility Level of Agents", "Agents", "Utility; ", "Without communication", xData, utilityData[1]);
        utilityChart.getStyler().setYAxisMax(0.8);
        utilityChart.getStyler().setYAxisMin(0.75);
        utilityWrapper = new SwingWrapper<>(utilityChart);
        utilityWrapper.displayChart();
    }

    public float getTotalEffort(){
        float total = 0.0f;
        for (Agent agent : agentList){
            total += agent.effort;
        }
        return total;
    }

    public void runSimWithComm() throws InterruptedException {

        int number_of_agents_making_agreement = 2;

        //init random number generator
        Random random = new Random();

        while (number_of_agents_making_agreement <= numberOfAgents){

            //prepare yData
            System.arraycopy(yData[number_of_agents_making_agreement - 2], 0, yData[number_of_agents_making_agreement - 1], 0, numberOfAgents);

            String effortSeries = " " + number_of_agents_making_agreement + " agents communicating";
            String utilitySeries = " " + number_of_agents_making_agreement + " agents communicating";
            effortChart.addSeries(effortSeries, xData, yData[number_of_agents_making_agreement-1], null);
            effortWrapper.repaintChart();
            utilityChart.addSeries(utilitySeries, xData, utilityData[number_of_agents_making_agreement-1], null);
            utilityWrapper.repaintChart();

            for (int i=0; i<numberOfRounds; i++){

                //pick random agents
                List<Integer> range = IntStream.rangeClosed(0, numberOfAgents-1).boxed().collect(Collectors.toList());
                Collections.shuffle(range);
                Integer[] agent_indices = Arrays.copyOfRange(range.toArray(new Integer[0]), 0, number_of_agents_making_agreement);
                //propose new effort levels and store utilites before
                double[] effort_changes = new double[number_of_agents_making_agreement];
                double[] utilityBefore = new double[number_of_agents_making_agreement];

                for (int j = 0; j<number_of_agents_making_agreement; j++){
                    effort_changes[j] = random.nextDouble() * step;
                    //store utility before applying effort changes to later evaluate whether effort change improves utility
                    utilityBefore[j] = agentList[agent_indices[j]].compute_utility();
                }

                //change efforts
                for (int j=0; j<number_of_agents_making_agreement; j++){
                    agentList[agent_indices[j]].effort -= effort_changes[j];
                    if (agentList[agent_indices[j]].effort < 0){
                        agentList[agent_indices[j]].effort = 0;
                    }
                }

                //if either agents loses utility after effort change, roll back effort changes
                boolean all_win = true;
                double[] utility_after = new double[number_of_agents_making_agreement];
                for (int j=0; j<number_of_agents_making_agreement; j++){
                    utility_after[j] = agentList[agent_indices[j]].compute_utility();
                    if (utilityBefore[j] > utility_after[j]){
                        all_win = false;
                        break;
                    }
                }

                if (!all_win){
                    for (int j=0; j<number_of_agents_making_agreement; j++){
                        agentList[agent_indices[j]].effort += effort_changes[j];
                    }
                }
                else {
                    //all agents win from effort change; add to our data for plotting and sleep
                    for (int j = 0; j < number_of_agents_making_agreement; j++) {
                        yData[number_of_agents_making_agreement - 1][agent_indices[j]] = agentList[agent_indices[j]].effort;
                    }
                    updateUtilities(utilityData[number_of_agents_making_agreement-1]);
                    Thread.sleep(lag_time / 5);

                    //update our plots
                    effortChart.updateXYSeries(effortSeries, xData, yData[number_of_agents_making_agreement - 1], null);
                    effortWrapper.repaintChart();
                    utilityChart.updateXYSeries(utilitySeries, xData, utilityData[number_of_agents_making_agreement-1], null);
                    utilityWrapper.repaintChart();
                }

            }
            System.out.printf("With %d agents making agreements\n", number_of_agents_making_agreement);
            printState();
            number_of_agents_making_agreement++;
        }
    }

    public void printState(){
        for (Agent agent: agentList){
            System.out.printf("Effort: %f\n", agent.effort);
        }
    }

    public void runSimWithoutComm() throws InterruptedException {

        for (int i=0; i<3; i++){
            //in every round, let every agent set its own effort in order to maximize its utility

            for (int j=0;j<numberOfAgents; j++){
                yData[0][j]=agentList[j].maximize_utility();
                updateUtilities(utilityData[1]);
                updateUtilities(utilityData[0]);

                //plot
                effortChart.updateXYSeries("Without communication", xData.clone(), yData[0], null);
                effortWrapper.repaintChart();
                utilityChart.updateXYSeries("Without communication", xData.clone(), utilityData[0], null);
                utilityWrapper.repaintChart();
                Thread.sleep(lag_time);
            }
        }

        //find Nash Equilibrium and Pareto optimum if all preferences are the same
        if (preference != 0){
            float nash_eq = ( (numberOfAgents + b - 1)* preference) / (numberOfAgents + (b-1) * preference);
            System.out.println("Nash Equilibrium effort is " + nash_eq);
            float pareto_opt = pareto_optimum_effort();
            float pareto_ut = agentList[0].compute_utility_with(pareto_opt, (numberOfAgents*pareto_opt));
            for (int i=0; i<numberOfAgents; i++){
                yDataPareto[i] = pareto_opt;
                utilityDataPareto[i] = pareto_ut;
            }
            System.out.println(pareto_opt);
            effortChart.addSeries("Pareto Optimum", xData, yDataPareto, null);
            effortWrapper.repaintChart();
            utilityChart.addSeries("Pareto Optimum", xData, utilityDataPareto, null);
            utilityWrapper.repaintChart();
        }
        printState();

    }

    private void updateUtilities(double[] utilities){
        for (int i=0;i<numberOfAgents; i++){
            utilities[i] = agentList[i].compute_utility();
        }
    }

    public float pareto_optimum_effort(){
        return b*preference/(b*preference-preference+1);
    }

    public static void main(String[] args) throws InterruptedException {

        //init agentPool and agents with parameters given on top of this file
        AgentPool pool = new AgentPool();

        //run simulation without allowing agents to make agreements
        pool.runSimWithoutComm();

        //run simulation allowing agreements between two agents
        pool.runSimWithComm();
    }
}
