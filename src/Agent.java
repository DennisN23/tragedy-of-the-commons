import java.lang.Math;

public class Agent {

    float effort;
    float preference;
    float last_utility;
    AgentPool agentPool;

    public Agent(float pref, AgentPool ap){
        effort = 0;
        preference = pref;
        last_utility = 0;
        agentPool = ap;
    }

    public Agent(AgentPool ap){
        effort = 0;
        preference = (float)Math.random();
        last_utility = 0;
        agentPool = ap;
    }

    //calculates the effort with which this agent achieves maximum utility given the current situation
    //NOTE: This is only the best effort for a specific situation. If efforts by other agents change,
    //this function should be called again.
    public float maximize_utility(){
        float effortOfTheOthers = agentPool.getTotalEffort() - effort;
        float b = agentPool.b;
        effort = (float) ( b * preference - effortOfTheOthers
                + Math.sqrt(
                        Math.pow(effortOfTheOthers - b * preference, 2)
                        + 4 * preference * effortOfTheOthers * (1 + preference * (b-1)))
                ) / (2 * (1 + preference * (b-1)));
        return effort;
    }

    public float compute_utility(){
        return (float) (Math.pow( (agentPool.a*effort) / (Math.pow(agentPool.getTotalEffort(), 1-agentPool.b)), preference)
                * Math.pow(1-effort, 1-preference));
    }

    public float compute_utility_with(float effort, float totalEffort){
        return (float) (Math.pow( (agentPool.a*effort) / Math.pow(totalEffort, 1-agentPool.b) , preference)
                * Math.pow(1-effort, 1-preference));
    }

}
